# Assignment-1: Unisearcher

This project is a submission to the first assignment in PROG-2005: Cloud Technologies.
Unisearcher is a REST web application in Golang that provides the client to retrieve information about universities that
may be candidates for application based on their name, alongside useful contextual information pertaining to the country
it is situated in.

[TOC]

## Endpoints

### Uniinfo

This endpoint returns information about universities that contains the given name,
and the country that the particular university is situated in.


```text
Method: GET
Path: /unisearcher/v1/uniinfo/
Request: uniinfo/{:partial_or_complete_university_name}{?{fields={:field1,field2,...}}&{limit={:number}}}
```

#### Parameters

```{:partial_or_complete_university_name}``` is the partial or complete university name of the 
universities to be searched for. 

```{?fields={:field1,field2,...}}``` is an optional parameter that specifies which fields that
should be included in the result. The available fields are ```name,country,isocode,webpages,languages,map```.
If no fields are specified, **all the fields are included**.

```{?limit={:number}}``` is an optional parameter that limits the number of universities (```number```) that are reported.
If no limit is specified, **all possible results are included**.

#### Response:

Content type: ```application/json```

Status codes:
- 200: Everything is OK.
- 204: The request found no results.
- 400: The request had wrong formatting.
- 404: A wrong path was used.
- 405: An not allowed method used.
- 500: Undefined server side error.

#### Example requests and responses

Request:
```uniinfo/molde```

```json
[
    {
        "name": "Molde University College",
        "country": "Norway",
        "isocode": "NO",
        "webpages": [
            "http://www.himolde.no/"
        ],
        "languages": {
            "nno": "Norwegian Nynorsk",
            "nob": "Norwegian Bokmål",
            "smi": "Sami"
        },
        "map": "https://www.openstreetmap.org/relation/2978650"
    }
]
```

Request:
```uniinfo/oslo?fields=name,country```

Response:
```json
[
  {
    "name": "Oslo School of Architecture",
    "country": "Norway"
  },
  {
    "name": "University of Oslo",
    "country": "Norway"
  }
]
```

Request:
```uniinfo/science?fields=map,name,languages&limit=1```

Response:
```json
[
  {
    "name": "Cooper Union for the Advancement of Science and Art",
    "languages": {
      "eng": "English"
    },
    "map": "https://www.openstreetmap.org/relation/148838#map=2/20.6/-85.8"
  }
]
```


### Neighbourunis

This endpoint provides an overview of universities in neighbouring countries to 
a given country that have the same name component (e.g., "Science") in their 
institution name.

```text
Method: GET
Path: /unisearcher/v1/neighbourunis/
Request: neighbourunis/{:country_name}/{:partial_or_complete_university_name}{?{fields={:field1,field2,...}}&{limit={:number}}}
```

#### Parameters

```{:country_name}``` refers to the English name for the country that is the basis (basis country) of 
the search of unis with the same name in neighbouring countries.

```{:partial_or_complete_university_name}``` is the partial or complete university name, for which 
universities with similar name are sought in neighbouring countries

```{?fields={:field1,field2,...}}``` is an optional parameter that specifies which fields that
should be included in the result. The available fields are ```name,country,isocode,webpages,languages,map```.
If no fields are specified, **all the fields are included**.

```{?limit={:number}}``` is an optional parameter that limits the number of universities (```number```) that are reported.
If no limit is specified, **all possible results are included**.

#### Response:

Content type: ```application/json```

Status codes:
- 200: Everything is OK.
- 204: The request found no results.
- 400: The request had wrong formatting.
- 404: A wrong path was used.
- 405: An not allowed method used.
- 500: Undefined server side error.

#### Example requests and responses

Request:
```neighbourunis/sweden/oslo```

Response:
```json
[
  {
    "name": "Oslo School of Architecture",
    "country": "Norway",
    "isocode": "NO",
    "webpages": [
      "http://www.aho.no/"
    ],
    "languages": {
      "nno": "Norwegian Nynorsk",
      "nob": "Norwegian Bokmål",
      "smi": "Sami"
    },
    "map": "https://www.openstreetmap.org/relation/2978650"
  },
  {
    "name": "University of Oslo",
    "country": "Norway",
    "isocode": "NO",
    "webpages": [
      "http://www.uio.no/"
    ],
    "languages": {
      "nno": "Norwegian Nynorsk",
      "nob": "Norwegian Bokmål",
      "smi": "Sami"
    },
    "map": "https://www.openstreetmap.org/relation/2978650"
  }
]
```

Request:
```neighbourunis/china/science?limit=1```

Response:
```json
[
  {
    "name": "Kabul Health Sciences Institute",
    "country": "Afghanistan",
    "isocode": "AF",
    "webpages": [
      "http://www.kabuli.edu.af/"
    ],
    "languages": {
      "prs": "Dari",
      "pus": "Pashto",
      "tuk": "Turkmen"
    },
    "map": "https://www.openstreetmap.org/relation/303427"
  }
]
```

Request:
```neighbourunis/norway/economics?fields=name&limit=4```

Response:
```json
[
  {
    "name": "Helsinki School of Economics and Business Administration"
  },
  {
    "name": "Swedish School of Economics and Business Administration, Finland"
  },
  {
    "name": "Turku School of Economics and Business Administration"
  },
  {
    "name": "Khabarovsk State Academy of Economics and Law"
  }
]
```

### Diag

The diagnostics interface indicates the availability of individual services 
this service depends on. The reporting occurs based on status codes returned
by the dependent services, and it further provides information about 
the uptime of the service.

```text
Method: GET
Path: /unisearcher/v1/diag/
```

#### Response

Content type: ```application/json```

Status codes:
- 200: Everything is ok.
- 404: Method not allowed.
- 500: Undefined server side error

Body:
```json
{
   "universitiesapi": "<http status code for universities API>",
   "countriesapi": "<http status code for restcountries API>",
   "version": "v1",
   "uptime": "<time in seconds from the last service restart>"
}

```

## Deployment

For ease of use, and for the submission of this project, the API is hosted on a heroku-instance,
located at:  
https://prog2005-unisearcher-vegarfae.herokuapp.com/unisearcher/v1/

The API can also be downloaded and run on your own machine. The instance will at default
run at ```localhost:8080/unisearcher/v1```, but the port can be changed in the ```main.go``` file.

## Design Choices

### Project structure

The project structure was created with the goal of responsibility driven design,
and to minimize code duplication overall.

![Dependencies-diagram](diagrams/Dependency-diagram.svg)

The endpoint-handlers got one file each, and are all located in the "handlers" package.
Since both uniinfo and neighbourunis need to request both countries and universities, 
the code to do this is abstracted to the "requests" package, in one file for country,
and one for university. 

Since both of these requests have much of the code in common, all code related to
requesting and getting a response is moved to an own request file.

The same thought-process was used to create the rest of the files, like queryUtility,
constants, structs, and uptime.

### Limiting API-requests

When designing the functions in the program, there was a goal to reduce the number of
requests sent to the API's to a reasonable limit. One way this was done, was caching, which 
is covered under the next headline. 

Another way to achieve this, was the structure of the neighbourunis endpoint.
To achieve the desired functionality of the endpoint, there was two main paths that I
saw as available:

Example request: ```neighbourunis/china/economics```

1. Request the Country
2. Get all borders from the country
3. For each border, request the country
   1. For each country, request universities with the correct name(16 requests) OR
   2. Request universities with the correct name, and filter out the ones from the wrong country(1 request)

I chose the latter, since it drastically reduced the number of requests sent to the 
university API. The downside is that the payload from the API can get quite large.
This reduces the load done on the API-server side, and increases the load this servers side,
which is in line with the REST-principles.

### Cache

Cache is another functionality implemented to reduce the amount of requests done to the
API, and in this case, the Country-API.

It works by storing the requested countries inside a cache, and if the
same country is requested again, it retrieves the country from the cache instead.

![Cache flowchart](diagrams/cache-flowchart.svg)

To prevent that entries get outdated, each time a cached entry is requested, it checks if 
the entry is over 50 days old. This ensured an updated cache.

## Extra features

- Query Fields: The user is able to specify which fields that should be included in the response.
- Query Limit: The user is able to specify the limit in **both** endpoints.
- Cache: The countries are cached, resulting in less API requests and shorter response time.

## Resolved edge cases

### Countries with inconsistent names

Since the API depends on two other API's, (https://restcountries.com/ and http://universities.hipolabs.com/)
the names of the different countries will not always be consistent.

Some examples:
```text
| RestcountriesAPI: | UniversitiesAPI:   |
|-------------------|--------------------|
| Vietnam           | Viet Nam           |
| Russia            | Russian Federation |
```

Problematic query example: ```unisearcher/v1/neighbourunis/china/science```

Previously, when doing this query in the API, the results from Vietnam (Viet nam) 
was not included.

This was fixed by refactoring the API to search for countries with the CCA2 or CCA3 code
when possible. This ensures that the correct country is requested.

### CountryAPI responding with array and object

The RestCountryAPI is not consistent with the response-type when responding with only
one country.

A request to ```https://restcountries.com/v3.1/alpha/BEN``` would result in a response 
consisting of one country-object, inside an array;

While a request to ```https://restcountries.com/v3.1/alpha/BEN?fields=name``` would
result in a response consisting of only a country-object, with no surrounding array.

This resulted in making it difficult to create on json decoder to handle all the
responses, since it had to decode into a different type (array or struct) depending on
the response.

The fix was to check what type the response was, before decoding it. This is done by
converting the whole response body into a byte-array, and checking if the first 
character was a ```[``` (array) or a ```{``` (object). This code can be seen
in ```requestCountry.go```


## Further improvements

### Internal error-handling

While the error-handling visible to the end-users of this API is sorted, the internals
of the error-handling could be improved. There is some dangling errors here and there,
and the strategy of handling errors could be more consistent. With more time, this 
would be the first task on my TODO-list.