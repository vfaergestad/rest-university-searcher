package constants

// File that defines constants that are being used throughout the program
const (
	// PORT Default port. Is set if the port is not given by the environment variables
	PORT = "8080"

	// The paths that will be handles by each handler
	DEFAULT_PATH       = "/unisearcher/"
	UNIINFO_PATH       = "/unisearcher/v1/uniinfo/"
	NEIGHBOURUNIS_PATH = "/unisearcher/v1/neighbourunis/"
	DIAG_PATH          = "/unisearcher/v1/diag/"

	// The URLS to the different API's
	UNIVERSITIESAPI_URL = "http://universities.hipolabs.com/"
	COUNTRIESAPI_URL    = "https://restcountries.com/v3.1/"

	// The standard fields to request from the country-API
	COUNTRIESAPI_ALL_STANDARD_FIELDS = "name,languages,maps,borders,cca2,cca3"

	// Different limit constants
	LIMIT_DEFAULT         = 0
	CACHE_AGE_LIMIT_HOURS = 1200

	LINK_TO_DOCS = "https://git.gvk.idi.ntnu.no/course/prog2005/prog2005-2022-workspace/vegarfae/assignment-1/-/blob/main/README.md"
)
