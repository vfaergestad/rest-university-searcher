package custom_errors

// File that includes functions that returns the custom errors. Ensures consistent error-handling.
import (
	"assignment-1/constants"
	"errors"
	"fmt"
)

func GetNotValidPathError() error {
	return errors.New(
		fmt.Sprintf("Not a valid endpoint. \n\n"+
			"Please use paths %s, %s, or %s. \n\n"+
			"%s", constants.DIAG_PATH, constants.UNIINFO_PATH, constants.NEIGHBOURUNIS_PATH, getDocumentationError().Error()),
	)
}

func getDocumentationError() error {
	return errors.New(fmt.Sprintf("See %s for documentation", constants.LINK_TO_DOCS))
}

func GetErrorDuringEncodingError() error {
	return errors.New("Error during encoding")
}

func GetNotValidLimitError() error {
	return errors.New("The limit is not a valid positive integer. No limit set")
}

func GetNoLimitGivenError() error {
	return errors.New("No limit given")
}

func GetNoCountriesFoundError() error {
	return errors.New("No Countries found")
}

func GetNoUniversitiesFoundError() error {
	return errors.New("No Universities found")
}

func GetBadNeighbourunisRequestError() error {
	return errors.New(fmt.Sprintf("Not a valid request. Format: neighbourunis/{:country_name}/{:partial_or_complete_university_name}{?{fields={:field1,field2,...}}&{limit={:number}}}\n"+
		"\n"+
		"%s", getDocumentationError().Error()))
}
