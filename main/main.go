package main

// Main file. Includes all the HandleFuncs and starting the webserver.
import (
	"assignment-1/constants"
	"assignment-1/uptime"
	"assignment-1/webserver/handlers"
	"log"
	"net/http"
	"os"
)

func main() {

	// Retrives and sets the port from the environment variable. Is necessary to ensure that Heroku functions properly.
	port := os.Getenv("PORT")
	if port == "" {
		log.Println("$PORT has not been set. Default: 8080")
		port = constants.PORT
	}

	// Points the different URL-paths to the correct handler
	http.HandleFunc(constants.DEFAULT_PATH, handlers.HandlerDefault)
	http.HandleFunc(constants.UNIINFO_PATH, handlers.HandlerUniInfo)
	http.HandleFunc(constants.NEIGHBOURUNIS_PATH, handlers.HandlerNeighbourUnis)
	http.HandleFunc(constants.DIAG_PATH, handlers.HandlerDiag)

	// Starting HTTP-server
	log.Println("Starting server on port " + port + " ...")
	uptime.Init()
	log.Fatal(http.ListenAndServe(":"+port, nil))

}
