package structs

// Includes the different structs used by the program, and the functions that acts upon them

import "time"

// Diagnose struct. Used to hold one instance of the diagnose for the DIAG handler
type Diagnose struct {
	UniversitiesApi string `json:"universitiesapi"`
	CountriesApi    string `json:"countriesapi"`
	Version         string `json:"version"`
	Uptime          string `json:"uptime"`
}

// UniAndCountry struct. Used to hold one instance of a combined country and university, and is the one that gets
// returned by server in the responses
type UniAndCountry struct {
	Name      string            `json:"name,omitempty"`
	Country   string            `json:"country,omitempty"`
	Isocode   string            `json:"isocode,omitempty"`
	WebPages  []string          `json:"webpages,omitempty"`
	Languages map[string]string `json:"languages,omitempty"`
	Map       string            `json:"map,omitempty"`
}

// University struct. Used to hold one instance of a university from the university-API
type University struct {
	Name         string   `json:"name"`
	Country      string   `json:"country"`
	AlphaTwoCode string   `json:"alpha_two_code"`
	WebPages     []string `json:"web_pages"`
}

// Country struct. Used to hold one instance of a country from the country-API
type Country struct {
	Name      map[string]interface{} `json:"name"`
	Languages map[string]string      `json:"languages"`
	Maps      map[string]string      `json:"maps"`
	Borders   []string               `json:"borders"`
	CCA3      string                 `json:"cca3"`
	CCA2      string                 `json:"cca2"`
	Cached    time.Time
}

/*
CombineUniCountry Combines one instance of a University struct with an instance of a Country struct into a
UniAndCountry struct.
Param u: University to combine.
Param c: Country to combine.
Returns: An UniAndCountry struct, consisting of the given u and c.
*/
func CombineUniCountry(u University, c Country, fields ...string) UniAndCountry {
	var uniInfo UniAndCountry
	// If fields are given, only include the given fields in the resulting struct
	if len(fields) > 0 {
		for _, f := range fields {
			switch f {
			case "name":
				uniInfo.Name = u.Name

			case "country":
				uniInfo.Country = u.Country

			case "isocode":
				uniInfo.Isocode = u.AlphaTwoCode

			case "webpages":
				uniInfo.WebPages = u.WebPages

			case "languages":
				uniInfo.Languages = c.Languages

			case "map":
				uniInfo.Map = c.Maps["openStreetMaps"]
			}
		}
	} else {
		// If no fields are given, include all fields in the resulting struct
		uniInfo = UniAndCountry{
			Name:      u.Name,
			Country:   u.Country,
			Isocode:   u.AlphaTwoCode,
			WebPages:  u.WebPages,
			Languages: c.Languages,
			Map:       c.Maps["openStreetMaps"],
		}
	}
	return uniInfo

}
