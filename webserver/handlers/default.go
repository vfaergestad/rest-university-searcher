package handlers

// Includes the default handler of the program.
import (
	"assignment-1/custom_errors"
	"net/http"
)

// HandlerDefault Default handler of the server. Returns an error message with the correct paths to use.
func HandlerDefault(w http.ResponseWriter, r *http.Request) {
	http.Error(w, custom_errors.GetNotValidPathError().Error(), http.StatusNotFound)
}
