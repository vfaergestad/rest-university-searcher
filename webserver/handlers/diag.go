package handlers

// The diag handler file. Handles all requesting incoming to unisearcher/diag
import (
	"assignment-1/constants"
	"assignment-1/structs"
	"assignment-1/uptime"
	"assignment-1/webserver/requests"
	"encoding/json"
	"fmt"
	"net/http"
)

/*
HandlerDiag Handler for the diag requests.
Param w: The http.ResponseWriter
Param r: The http.Request pointer.
*/
func HandlerDiag(w http.ResponseWriter, r *http.Request) {

	// Responds with error if method is anything else than GET.
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported. Currently only GET are supported.", http.StatusMethodNotAllowed)
		return
	}

	diagnose := requestDiagnose()

	w.Header().Add("content-type", "application/json")

	encoder := json.NewEncoder(w)
	// Ensures that the response looks like a JSON structure in browsers
	encoder.SetIndent("", "\t")
	err := encoder.Encode(diagnose)
	if err != nil {
		http.Error(w, "Error during encoding", http.StatusInternalServerError)
		return
	}

}

/*
requestDiagnose creates a diagnose response by requesting the status codes from the university and country API's.
Returns: a Diagnose struct with the response
*/
func requestDiagnose() structs.Diagnose {

	url := constants.UNIVERSITIESAPI_URL

	// Retrieves a response from the university-API with the requests package.
	universityApiRes, err := requests.CreateAndDoRequest(http.MethodHead, url)
	if err != nil {

	}

	url = constants.COUNTRIESAPI_URL + "all"

	// Retrieves a response from the country-API with the requests package.
	countryAPIRes, err := requests.CreateAndDoRequest(http.MethodHead, url)
	if err != nil {

	}

	universityApiStatus := universityApiRes.StatusCode
	countriesApiStatus := countryAPIRes.StatusCode

	return structs.Diagnose{
		UniversitiesApi: fmt.Sprintf("%d", universityApiStatus),
		CountriesApi:    fmt.Sprintf("%d", countriesApiStatus),
		Version:         "v1",
		Uptime:          fmt.Sprintf("%ds", uptime.GetUptime()),
	}

}
