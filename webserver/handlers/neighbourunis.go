package handlers

import (
	"assignment-1/custom_errors"
	"assignment-1/structs"
	"assignment-1/webserver/query_utility"
	"assignment-1/webserver/requests/req_country"
	"assignment-1/webserver/requests/req_university"
	"encoding/json"
	"net/http"
	"path"
	"strings"
)

/*
HandlerNeighbourUnis Handler for the NeighbourUnis requests.
Param w: The http.ResponseWriter
Param r: The http.Request pointer.
*/
func HandlerNeighbourUnis(w http.ResponseWriter, r *http.Request) {

	// Responds with error if method is anything else than GET.
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported. Currently only GET are supported.", http.StatusMethodNotAllowed)
		return
	}

	// Retrieves the search-terms from the path
	cleanPath := path.Clean(r.URL.Path)
	pathList := strings.Split(cleanPath, "/")

	countryQuery := pathList[len(pathList)-2]
	uniQuery := pathList[len(pathList)-1]

	if len(pathList) != 6 {
		http.Error(w, custom_errors.GetBadNeighbourunisRequestError().Error(), http.StatusBadRequest)
		return
	}

	// Retrieves the specified fields from the query
	fields := query_utility.GetFields(r.URL)

	// Retrieves the limit from the query
	limit, err := query_utility.GetLimit(r.URL)
	if err != nil {
		http.Error(w, custom_errors.GetNotValidLimitError().Error(), http.StatusBadRequest)
		return
	}

	// Retrieves the country to find neighbouring countries to
	country, err := req_country.GetCountryByName(countryQuery)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNoContent)
		return
	}

	// Retrieves all neighbouring countries from the "borders" field in the country struct.
	var countries []structs.Country
	for _, bor := range country.Borders {
		c, _ := req_country.GetCountryByAlpha(bor)
		countries = append(countries, c)
	}

	// Retrieves all alphaTwoCodes from the bordering countries, and appends them to a list
	var alphaTwoCodes []string
	for _, c := range countries {
		alphaTwoCodes = append(alphaTwoCodes, c.CCA2)
	}

	// Finds all universities with the given name-search, and retrieves the ones with the matching alphaTwoCode,
	// before combining them with the correct country
	universities, err := req_university.RequestUniversityByName(uniQuery)
	var uniInfo []structs.UniAndCountry
	for _, u := range universities {
		for _, a := range alphaTwoCodes {
			if u.AlphaTwoCode == a {
				c, _ := req_country.GetCountryByAlpha(a)
				// If the list of results is smaller than the limit, append the university.
				// If not, break out of the loops.
				if limit == 0 || len(uniInfo) < limit {
					uniInfo = append(uniInfo, structs.CombineUniCountry(u, c, fields...))
					break
				} else {
					break
				}
			}
		}
		if len(uniInfo) >= limit && limit != 0 {
			break
		}
	}

	if len(uniInfo) == 0 {
		http.Error(w, custom_errors.GetNoUniversitiesFoundError().Error(), http.StatusNoContent)
		return
	}
	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	// Ensures that the response looks like a json file in the browser.
	encoder.SetIndent("", "\t")
	err = encoder.Encode(uniInfo)
	if err != nil {
		http.Error(w, "Error during encoding", http.StatusInternalServerError)
		return
	}

}
