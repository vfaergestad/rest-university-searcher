package handlers

import (
	"assignment-1/custom_errors"
	"assignment-1/structs"
	"assignment-1/webserver/query_utility"
	"assignment-1/webserver/requests/req_country"
	"assignment-1/webserver/requests/req_university"
	"encoding/json"
	"fmt"
	"net/http"
	"path"
)

/*
HandlerUniInfo Handler for the UniInfo requests.
Param w: The http.ResponseWriter
Param r: The http.Request pointer.
*/
func HandlerUniInfo(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported. Currently only GET are supported.", http.StatusMethodNotAllowed)
	}

	universities := getUniInfo(r)

	if len(universities) == 0 {
		http.Error(w, "No universities found", http.StatusNoContent)
		return
	}

	// Retrieves the specified fields from the query
	fields := query_utility.GetFields(r.URL)

	// Retrieves the limit from the query
	limit, err := query_utility.GetLimit(r.URL)
	if err != nil {
		// Adds not valid limit error to response
		if err.Error() == custom_errors.GetNotValidLimitError().Error() {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	// Retrieves the combined universities with their correct country
	combinedInfo := getCombined(universities, fields, limit)

	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	err = encoder.Encode(combinedInfo)
	if err != nil {
		http.Error(w, custom_errors.GetErrorDuringEncodingError().Error(), http.StatusInternalServerError)
		return
	}

}

func getUniInfo(r *http.Request) []structs.University {
	// Retrieves the search-string from the path
	search := path.Base(r.URL.Path)
	if search == "" {
		return nil
	}

	// Retrieves the resulting universities with the request package.
	universities, _ := req_university.RequestUniversityByName(search)

	if universities == nil {
		return nil
	}

	return universities

}

/*
getCombined combines a list of universities with their corresponding country.
Param universities: The universities to combine.
Param fields: The fields to be included in the combined structs
Param limit: The number of results wanted in the response
Returns: a list of combined universities and countries
*/
func getCombined(universities []structs.University, fields []string, limit int) []structs.UniAndCountry {
	var uniInfoList []structs.UniAndCountry
	for _, u := range universities {
		c, err := req_country.GetCountryByAlpha(u.AlphaTwoCode)
		if err != nil {
			fmt.Println(err)
		}
		uniInfo := structs.CombineUniCountry(u, c, fields...)
		if limit == 0 || len(uniInfoList) < limit {
			uniInfoList = append(uniInfoList, uniInfo)
		} else {
			break
		}

	}
	return uniInfoList
}
