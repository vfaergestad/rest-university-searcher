package query_utility

// File that holds the functions used to retrieve different queries from a URL.
import (
	"assignment-1/constants"
	"assignment-1/custom_errors"
	"net/url"
	"strconv"
	"strings"
)

/*
GetLimit Retrieves the limit from the query in a URL
Param URL: URL to retrieve the limit from
Returns the limit retrieved, or an error
*/
func GetLimit(URL *url.URL) (int, error) {
	var limit int
	var returnError error
	if URL.Query()["limit"] != nil {
		if l, err := strconv.Atoi(URL.Query()["limit"][0]); err != nil || l < 0 {
			returnError = custom_errors.GetNotValidLimitError()
			limit = constants.LIMIT_DEFAULT
		} else {
			limit = l
		}
	} else {
		limit = constants.LIMIT_DEFAULT
		returnError = custom_errors.GetNoLimitGivenError()
	}
	return limit, returnError
}

/*
GetFields Retrieves the fields from the query in a URL
Param URL: URL to retrieve the fields from
Returns the fields retrieved, or an error
*/
func GetFields(URL *url.URL) []string {
	var fields []string
	if URL.Query().Get("fields") != "" {
		fields = strings.Split(URL.Query().Get("fields"), ",")
	} else {
		fields = nil
	}
	return fields
}
