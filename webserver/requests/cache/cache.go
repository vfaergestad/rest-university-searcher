package cache

// File that holds the cache, and the functions that acts upon it
import (
	"assignment-1/constants"
	"assignment-1/structs"
	"errors"
	"fmt"
	"strings"
	"time"
)

var countryCache = make(map[string]structs.Country)

/*
AddCountry Adds a new country to the cache, if it's not present already.
Param c: Country to be added to the cache.
Returns: Error if the country is in the cache already.
*/
func AddCountry(c structs.Country) error {
	var cName string
	cName = strings.Title(c.Name["common"].(string))
	//log.Printf("Adding %s to cache", cName)
	if _, exists := countryCache[cName]; !exists {
		c.Cached = time.Now()
		countryCache[cName] = c
		//log.Printf("Added %s to cache", cName)
		return nil
	} else {
		return errors.New(fmt.Sprintf("%s is already in the cache.", cName))
	}
}

/*
GetCountry Tries to retrieve a country from the cache. If the country is not present, or older than 50 days, an empty
struct and an error is returned.
Param name: Name of country to add to the cache.
Returns: Country if cached, or an empty struct and an error.
*/
func GetCountry(name string) (structs.Country, error) {
	//log.Printf("Trying to get %s from cache", name)
	if c, exists := countryCache[strings.Title(name)]; exists {
		if time.Since(c.Cached).Hours() > constants.CACHE_AGE_LIMIT_HOURS {
			// Deletes the country from the cache if the entry is older than the limit
			delete(countryCache, c.Name["common"].(string))
			return structs.Country{}, errors.New(fmt.Sprintf("%s is in cache, but is over 50 days old"))
		}
		//log.Printf("Got %s from cache", name)
		return c, nil
	} else {
		//log.Printf("%s not in cache", name)
		return structs.Country{}, errors.New(fmt.Sprintf("%s is not in the cache.", name))
	}
}

/*
GetCountryByAlpha Tries to retrieve a country from the cache by its alpha code, either CCA2 og CCA3.
If the country is not present, or older than the limit, an empty struct and an error is returned.
Param name: Alpha code of country to add to the cache. Either CCA2 og CCA3
Returns: Country if cached, or an empty struct and an error.
*/
func GetCountryByAlpha(alpha string) (structs.Country, error) {
	for _, c := range countryCache {
		if strings.ToUpper(c.CCA3) == strings.ToUpper(alpha) {
			//log.Printf("Got %s from cache", c.CCA3)
			if time.Since(c.Cached).Hours() > constants.CACHE_AGE_LIMIT_HOURS {
				// Deletes the country from the cache if the entry is older than the limit
				delete(countryCache, c.Name["common"].(string))
				return structs.Country{}, errors.New(fmt.Sprintf("%s is in cache, but is over 50 days old"))
			}
			return c, nil
		} else if strings.ToUpper(c.CCA2) == strings.ToUpper(alpha) {
			if time.Since(c.Cached).Hours() > constants.CACHE_AGE_LIMIT_HOURS {
				// Deletes the country from the cache if the entry is older than the limit
				delete(countryCache, c.Name["common"].(string))
				return structs.Country{}, errors.New(fmt.Sprintf("%s is in cache, but is over 50 days old"))
			}
			//log.Printf("Got %s from cache", c.CCA2)
			return c, nil
		}
	}
	return structs.Country{}, errors.New(fmt.Sprintf("%s is not in the cache.", alpha))
}
