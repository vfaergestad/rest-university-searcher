package req_country

import (
	"assignment-1/constants"
	"assignment-1/custom_errors"
	"assignment-1/structs"
	"assignment-1/webserver/requests"
	"assignment-1/webserver/requests/cache"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

/*
GetCountryByName Retrieves the country specified by the name. Tries to get the country from cache, and retrieves it
from the API if it's not present.
Param name: Name of the country to retrieve.
Returns: The country to retrieve, if found, and an error, if rasied.
*/
func GetCountryByName(name string) (structs.Country, error) {
	//Tries to get country from cache
	if c, err := cache.GetCountry(name); err == nil {
		return c, nil
	} else {
		// Country not in cache, retrieves from the API instead
		url := fmt.Sprintf("%sname/%s?fields=%s", constants.COUNTRIESAPI_URL, name, constants.COUNTRIESAPI_ALL_STANDARD_FIELDS)
		var countries []structs.Country
		if countries, err = requestCountry(url); err != nil {
			return structs.Country{}, err
		}

		// Goes through the list of retrieved country to find the correct one.
		for _, c := range countries {
			if strings.ToLower(c.Name["common"].(string)) == strings.ToLower(name) ||
				strings.ToLower(c.Name["official"].(string)) == strings.ToLower(name) {
				// Adds the newly retrieved country to the cache
				if err = cache.AddCountry(c); err != nil {
					log.Println(err)
				}
				return c, nil
			}
		}

		return structs.Country{}, errors.New(fmt.Sprintf("%s not found in result", name))
	}

}

/*
GetCountryByAlpha Retrieves the country specified by the AlphaCode, either CCA2 or CCA3.
Tries to get the country from cache, and retrieves it from the API if it's not present.
Param AlphaCode: AlphaCode of the country to retrieve, either as CCA2 or CCA3.
Returns: The country to retrieve, if found, and an error, if rasied.
*/
func GetCountryByAlpha(alpha string) (structs.Country, error) {
	// Tries to get country from cache
	if c, err := cache.GetCountryByAlpha(alpha); err == nil {
		return c, nil
	} else {
		// Country not found in cache, retrieves from the API instead
		url := fmt.Sprintf("%salpha/%s?fields=%s", constants.COUNTRIESAPI_URL, alpha, constants.COUNTRIESAPI_ALL_STANDARD_FIELDS)
		var countries []structs.Country
		if countries, err = requestCountry(url); err != nil {
			return structs.Country{}, err
		}

		for _, c := range countries {
			if c.CCA3 == alpha || c.CCA2 == alpha {
				// Adds the newly retrieved country to the cache
				if err = cache.AddCountry(c); err != nil {
					log.Println(err)
				}
				return c, nil
			}
		}

		return structs.Country{}, errors.New(fmt.Sprintf("%s not found in result", alpha))
	}
}

/*
requestCountry Requests a country with the given URL, and decodes it.
Param URL: Url to use in the request.
Returns: List of countries that was in the response
*/
func requestCountry(url string) ([]structs.Country, error) {
	res, err := requests.CreateAndDoRequest(http.MethodGet, url)
	if err != nil {
		return nil, err
	}

	var c []structs.Country
	if c, err = decodeCountry(res); err != nil {
		return nil, err
	}

	return c, nil
}

/*
decodeCountry Decodes a response into a list of countries.
Param res: Response to decode.
Returns: A list of decodes countries if successful, an empty list and an error if not.
Inspiration from this answer: https://stackoverflow.com/a/55014220
*/
func decodeCountry(res *http.Response) ([]structs.Country, error) {

	//Converts the response body into a bytearray
	data, _ := ioutil.ReadAll(res.Body)
	x := bytes.TrimLeft(data, " \t\r\n")
	//Checks if the body is an json array.
	isArray := len(x) > 0 && x[0] == '['

	var countries []structs.Country
	// If the body is an array, it decodes the body into a list of countries
	if isArray {
		if err := json.Unmarshal(data, &countries); err != nil {
			return nil, err
		}
		return countries, nil
	} else {
		// If the body is an object, it decodes the body into an country struct, and appends it to the countries-list
		var country structs.Country
		if err := json.Unmarshal(data, &country); err != nil {
			return nil, err
		}
		if country.Name == nil {
			return countries, custom_errors.GetNoCountriesFoundError()
		}
		countries = append(countries, country)
		return countries, nil
	}
}
