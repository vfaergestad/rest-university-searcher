package req_university

// File that includes the functions needed to request universities form the cache and the university-API
import (
	"assignment-1/constants"
	"assignment-1/structs"
	"assignment-1/webserver/requests"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

/*
RequestUniversityByName Requests a University by the given name.
Param search: University-name to search for.
Return: A array of found universities, or an empty array and an error.
*/
func RequestUniversityByName(search string) ([]structs.University, error) {
	//fmt.Println(url)

	query := "search?name_contains=" + search
	url := constants.UNIVERSITIESAPI_URL + query

	return requestUniversity(url)

}

/*
requestUniversity Requests universities from the University-API using a given URL.
Param url: URL to use in the request.
Returns: A array of found universities, or an empty array and an error.
*/
func requestUniversity(url string) ([]structs.University, error) {
	res, err := requests.CreateAndDoRequest(http.MethodGet, url)
	if err != nil {
		return nil, err
	}

	switch {
	case res.StatusCode == http.StatusNotFound:
		return []structs.University{}, errors.New(fmt.Sprintf("%d University not found", res.StatusCode))
	case res.StatusCode != http.StatusOK:
		return []structs.University{}, errors.New(fmt.Sprintf("Status code returned from universityAPI: %d", res.StatusCode))
	}

	var universities []structs.University
	if universities, err = decodeUniversities(res); err != nil {
		return nil, err
	}

	return universities, nil
}

/*
decodeUniversities Decodes a given response into an array of universities.
Param res: Response to decode.
Returns: A array of decoded universities, or an empty array and an error.
*/
func decodeUniversities(res *http.Response) ([]structs.University, error) {
	decoder := json.NewDecoder(res.Body)
	var universities []structs.University
	if err := decoder.Decode(&universities); err != nil {
		log.Println(err)
		return nil, err

	}
	return universities, nil
}
