package requests

import (
	"net/http"
	"strings"
)

/*
CreateAndDoRequest creates and does an HTTP-request, using the given method and url.
Param method: Method to use in the HTTP-request.
Param url: Url to use in the HTTP-request.
Returns: An http.response or an error.
*/
func CreateAndDoRequest(method string, url string) (*http.Response, error) {
	url = strings.ReplaceAll(url, " ", "%20")
	//fmt.Println(url)
	r, err := http.NewRequest(method, url, nil)
	if err != nil {
		return nil, err
	}

	r.Header.Add("content-type", "application/json")

	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		return nil, err
	}

	return res, nil
}
